#!/home/cenciso/miniconda3/envs/pangeo/bin/python
# -*- coding:utf-8 -*-
#---------------------------------------------------------------------------
# Project: /home/cenciso/Documents/RESEARCH/SENAMHI/2020/myconda_pkg
# File: /home/cenciso/Documents/RESEARCH/SENAMHI/2020/myconda_pkg/try.py
# @Author: Carlos Enciso Ojeda
# @Email: carlos.enciso.o@gmail.com
# @Created Date: Monday, October 12th 2020, 11:15:02 am
# -----
# @Last Modified: Monday, October 12th 2020 11:22:03 am
# Modified By: Carlos Enciso Ojeda at <carlos.enciso.o@gmail.com>
# -----
# Copyright (c) 2020 Peruvian Geophysical Institute
# @License: MIT
# -----
# HISTORY:
# Date      	By	Comments
# ----------	---	---------------------------------------------------------
#---------------------------------------------------------------------------
import numpy as np
import cmocean as cm
import pandas as pd
import xarray as xr
import geopandas as gpd
import matplotlib.pyplot as plt
import os
plt.style.use('ggplot')
#-------------------------------------#
# mydecorator 
#-------------------------------------#
def _decorator(func):
    def wrap(*args, **kwargs):
        print('#'+'-'*(len(*args)+2))
        func(*args)
        print('#'+'-'*(len(*args)+2))
    return wrap
@_decorator
def printing(msg):
    print('# '+msg)
#-------------------------------------#
#  
#-------------------------------------#
